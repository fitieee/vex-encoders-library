/*
  vex-encoder.h - Library for vex encoders.
  Created by Chris Woodle, January 31, 2016.
  Released into the public domain.
*/
#ifndef VexEncoder_h
#define VexEncoder_h

#include "Arduino.h"

class VexEncoder
{
public:
	VexEncoder(int pinA, int pinB);
	int encoderValue;
	void clearValue();
	void updateEncoder();
private:
	int encoderPinA;
	int encoderPinB;
	int currentPinAValue;
	int currentPinBValue;
	int encoderPinALastValue;
	int encoderPinBLastValue;
};

#endif
