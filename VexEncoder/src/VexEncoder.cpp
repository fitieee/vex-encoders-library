#include "Arduino.h"
#include "VexEncoder.h"
VexEncoder::VexEncoder(int pinA, int pinB) {
	pinMode(pinA, INPUT);
	pinMode(pinB, INPUT);
	encoderPinA = pinA;
	encoderPinB = pinB;
	encoderValue = 0;
	encoderPinALastValue = LOW;
	encoderPinBLastValue = LOW;
}

void VexEncoder::clearValue() {
	encoderValue = 0;
}

void VexEncoder::updateEncoder() {
	currentPinAValue = digitalRead(encoderPinA);     // Read Encoder0PinA, forward rotation
	currentPinBValue = digitalRead(encoderPinB);     // Read Encoder0PinB, reverse rotation

	if ((currentPinAValue == HIGH) && (encoderPinALastValue == LOW) && (currentPinBValue == LOW)) {
		encoderValue++;		
	}
	if ((currentPinAValue == HIGH) && (encoderPinALastValue == LOW) && (currentPinBValue == HIGH)) {
		encoderValue--;		
	}
	if ((currentPinBValue == HIGH) && (encoderPinBLastValue == LOW) && (currentPinAValue == LOW)) {
		encoderValue--;
	}
	if ((currentPinBValue == HIGH) && (encoderPinBLastValue == LOW) && (currentPinAValue == HIGH)) {
		encoderValue++;
	}
	if ((currentPinAValue == LOW) && (encoderPinALastValue == HIGH) && (currentPinBValue == LOW)) {
		encoderValue--;
	}
	if ((currentPinAValue == LOW) && (encoderPinALastValue == HIGH) && (currentPinBValue == HIGH)) {
		encoderValue++;
	}
	if ((currentPinBValue == LOW) && (encoderPinBLastValue == HIGH) && (currentPinAValue == LOW)) {
		encoderValue++;
	}
	if ((currentPinBValue == LOW) && (encoderPinBLastValue == HIGH) && (currentPinAValue == HIGH)) {
		encoderValue--;
	}

	encoderPinALastValue = currentPinAValue;
	encoderPinBLastValue = currentPinBValue;
}

